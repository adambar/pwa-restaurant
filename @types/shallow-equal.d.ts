declare module "shallow-equal/arrays" {
    function shallowEqualArrays(left?: any[], right?: any[]): boolean
    export = shallowEqualArrays
}