import {html} from "lit-html";

import 'bulma/css/bulma.css';

import App, {Component, RoutingConfiguration} from "./app";
import errorPage from "./layout/errorPage";
import {notification, promptNotification, successNotification} from "./utils/notification";
import header from "./layout/header";
import {get} from "./apiClient";
import Menu, {RawMenu} from "./menu/menu";
import AppState from './appState'
import '../@types/chrome-android'

const routing: RoutingConfiguration<AppState> = (app, router) => {
    router('/menu', (ctx) => app.routeDynamicallyTo(ctx, () => import("./menu/menuPage")))
    router('/orders', (ctx) => app.routeDynamicallyTo(ctx, () => import("./orders/ordersPage")))
    router('/', (ctx) => router.redirect('/menu?' + ctx.querystring))
    router('*', (ctx) => app.routeTo(ctx, errorPage))
}

const appContainer: Component<AppState> = (app, renderedRoute: Component<AppState>) => {
    app.useEffect(appContainer, () => {
        get<RawMenu>("/menu")
            .then((menu) => app.setState({menu: new Menu(menu)}))

    }, [])

    return html`
        ${app.state.notification ? notification(app) : ''}
    
        ${header(app)}
        
        <main class="container">
            ${renderedRoute}
        </main>
    `
}

const app = new App(routing, appContainer)

app.onInitialRender(() => {
    if (app.routeContext.querystring.indexOf("source=homescreen") !== -1) {
        app.setState({notification: successNotification("Hello, loyal user!")})
        app.routing.redirect(app.routeContext.path.split('?')[0])
    }
})

function invokeServiceWorkerUpdateFlow(registration: ServiceWorkerRegistration) {
    app.setState({
        notification: promptNotification(
            "New version of the app is available. Refresh now?",
            () => {
                if (registration.waiting) {
                    // let waiting Service Worker know it should became active
                    registration.waiting.postMessage('skip waiting')
                }
            })
    })
}

// check if the browser supports serviceWorker at all
if ('serviceWorker' in navigator) {
    // wait for the page to load
    window.addEventListener('load', async () => {
        // register the service worker from the file specified
        const registration = await navigator.serviceWorker.register('/service-worker.js')

        // ensure the case when the updatefound event was missed is also handled
        // by re-invoking the prompt when there's a waiting Service Worker
        if (registration.waiting) {
            invokeServiceWorkerUpdateFlow(registration)
        }

        // detect Service Worker update available and wait for it to become installed
        registration.addEventListener('updatefound', () => {
            // we need to keep the reference because this Service Worker will transition to waiting in a moment
            const installingWorker = registration.installing

            if (installingWorker) {
                // wait until the new Service worker is actually installed (ready to take over)
                installingWorker.addEventListener('statechange', () => {
                    if (installingWorker!.state === 'installed') {
                        // if there's an existing controller (previous Service Worker), show the prompt
                        if (navigator.serviceWorker.controller) {
                            invokeServiceWorkerUpdateFlow(registration)
                        } else {
                            // otherwise it's the first install, nothing to do
                            console.log('Service Worker initialized for the first time')
                        }
                    }
                })
            }
        })

        let refreshing = false;

        // detect controller change and refresh the page
        navigator.serviceWorker.addEventListener('controllerchange', () => {
            if (!refreshing) {
                window.location.reload()
                refreshing = true
            }
        })
    })

    let savedPromptEvent: BeforeInstallPromptEvent | undefined

    window.addEventListener('beforeinstallprompt', (event) => {
        // disable the default behavior for Chrome <=67
        event.preventDefault()

        // ensure the banner is not shown too often - Chrome on Android fires the event many times, if not reacted on
        const beforeInstallPromptShownAt = localStorage.getItem("beforeInstallPromptShown")
        if (beforeInstallPromptShownAt) {
            const nowTime = new Date().getTime()
            const promptShownTime = new Date(beforeInstallPromptShownAt).getTime()
            const daysDivisor = 1000 * 60 * 60 * 24
            const showLaterThanFewDaysAgo = ((nowTime - promptShownTime) / daysDivisor) < 7

            if (showLaterThanFewDaysAgo) {
                // do not show the banner too often
                return
            }
        }

        // store the event to use after the user accepts via our custom UI
        savedPromptEvent = event

        // show custom UI (in our case, notification)
        app.setState({
            notification: promptNotification(
                "Your Italian goodies taste better from your home screen. Do you want to try?",
                async () => {
                    // when user confirms in our custom UI, trigger the system prompt via the saved event
                    savedPromptEvent!.prompt()

                    // check the outcome of the prompt (optionally)
                    const outcome = await savedPromptEvent!.userChoice
                    console.log("Add to homescreen prompt outcome: ", outcome)

                    // clear the event object - no longer needed
                    savedPromptEvent = undefined
                },
                async () => {
                    localStorage.setItem("beforeInstallPromptShown", new Date().toISOString())
                })
        })
    })
}
