import {Notification} from './utils/notification'
import Menu from './menu/menu'
import {Order} from './orders/order'

export default interface AppState {
    notification?: Notification,
    menu: Menu,
    currentOrder?: Order,
}