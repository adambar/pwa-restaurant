declare global {
    interface Window {
        msCrypto?: Crypto
    }
}

export default function generateRandomId(size: number): string {
    const crypto = window.crypto || window.msCrypto
    const alphabet = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz-'

    let id = ''
    const bytes = crypto.getRandomValues(new Uint8Array(size))
    while (0 < size--) {
        id += alphabet[bytes[size] & 63]
    }
    return id
}
