import {html} from "lit-html";
import {classMap} from "lit-html/directives/class-map";
import {styleMap} from "lit-html/directives/style-map";

import App from '../app'
import AppState from '../appState'

export interface Notification {
    type: 'success' | 'danger' | 'info'
    message: string
    onConfirmed?: () => void
    onDismissed?: () => void
}

export function successNotification(message: string): Notification {
    return {
        type: 'success',
        message
    }
}

export function promptNotification(question: string, onConfirmed: () => void, onDismissed?: () => void): Notification {
    return {
        type: 'info',
        message: question,
        onConfirmed,
        onDismissed
    }
}

export function notification(app: App<AppState>) {
    if (app.state.notification) {
        const {message, type, onConfirmed, onDismissed} = app.state.notification

        const closeNotification = () => {
            app.setState({notification: undefined})
            if (onDismissed) {
                onDismissed()
            }
        }

        const confirmPrompt = () => {
            onConfirmed!()
            closeNotification()
        }

        app.useEffect(notification, () => {
            if (!onConfirmed) {
                const timeout = setTimeout(closeNotification, 2000)
                return () => clearTimeout(timeout)
            }
        }, [app.state.notification])

        return html`
            <div class=${classMap({notification: true, [`is-${type}`]: true})}
                 style=${styleMap({position: 'fixed', zIndex: '100', width: '100%'})}>
                ${onConfirmed
                    ? html`<a @click=${confirmPrompt}>${message}</a>`
                    : message
                }
                
                <button class="delete" type="button" @click=${closeNotification}></button>
            </div>
        `
    }

    return html``
}