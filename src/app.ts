import {render, TemplateResult} from "lit-html";
import page from "page";
import shallowEqualArrays from "shallow-equal/arrays"

export type RoutingConfiguration<S extends object> = (app: App<S>, router: PageJS.Static) => void
export type Component<S extends object> = (app: App<S>, ...params: any[]) => TemplateResult
export type AppContainer<S extends object> = (app: App<S>, renderedRoute: TemplateResult) => TemplateResult
export type Discriminator<S extends object> = Component<S> | { id: number | string }
export type EffectCleanup = void | (() => void)

export default class App<S extends object> {
    private readonly _appContainer: (app: App<S>, renderedRoute: TemplateResult) => TemplateResult
    private readonly _localState: WeakMap<Discriminator<S>, any>
    private readonly _effects: WeakMap<Discriminator<S>, any[]>
    private readonly _effectCleanups: WeakMap<Discriminator<S>, EffectCleanup>

    private _route!: Component<S>
    private _onInitialRenderCallback: () => void = () => {}

    public readonly routing: PageJS.Static
    public state: Partial<S>
    public routeContext!: PageJS.Context

    constructor(routingConfiguration: RoutingConfiguration<S>, appContainer: AppContainer<S>) {
        this._appContainer = appContainer

        this._localState = new WeakMap()
        this._effects = new WeakMap()
        this._effectCleanups = new WeakMap()

        this.state = {}

        this.routing = page
        routingConfiguration(this, page)
        this.routing()
    }

    routeTo(routeContext: PageJS.Context, route: Component<S>): void {
        this._route = route
        this.routeContext = routeContext
        this._render()
        this._onInitialRenderCallback()
    }

    async routeDynamicallyTo(routeContext: PageJS.Context, routeLoader: () => Promise<{ default: Component<S> }>): Promise<void> {
        const route = await routeLoader()
        this.routeTo(routeContext, route.default)
    }

    setState<U extends keyof S>(update: Pick<S, U>): void {
        this.state = Object.assign(this.state, update)
        this._render()
    }

    useLocalState<T>(discriminator: Discriminator<S>, defaultValue: T): [T, (newValue: T) => void] {
        if (!this._localState.has(discriminator)) {
            this._localState.set(discriminator, defaultValue)
        }

        return [
            this._localState.get(discriminator),
            (newValue: T) => {
                this._localState.set(discriminator, newValue)
                this._render()
            }
        ]
    }

    useEffect(discriminator: Discriminator<S>, effect: () => EffectCleanup, dependencies: any[]): void {
        if (!this._effects.has(discriminator) || !shallowEqualArrays(this._effects.get(discriminator), dependencies)) {
            const cleanup = this._effectCleanups.get(discriminator)

            if (cleanup) {
                cleanup()
            }

            this._effectCleanups.set(discriminator, effect())
            this._effects.set(discriminator, dependencies)
        }
    }

    onInitialRender(callback: () => void) {
        this._onInitialRenderCallback = callback
    }

    private _render(): void {
        if (this._route) {
            render(this._appContainer(this, this._route(this)), document.getElementById('root')!)
        } else {
            console.warn("No this._route set, can't render")
        }
    }
}
