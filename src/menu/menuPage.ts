import {html} from 'lit-html';

import menuSection from "./menuSection";
import {Component} from '../app'
import AppState from '../appState'

const menuPage: Component<AppState> = (app) => {
    return html`
        <h1 class="title">Menu</h1>
        
        ${app.state.menu
            ? app.state.menu.sections.map((section) => menuSection(app, section, app.state.menu!.dishesInSection(section)))
            : html`<p>Loading...</p>`}
    `
}

export default menuPage