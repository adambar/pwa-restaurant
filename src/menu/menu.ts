import keyBy from "lodash.keyby"
import flatten from "lodash.flatten"
import values from "lodash.values"
import {Dictionary} from "lodash";

export interface Dish {
    id: number
    name: string
    price: number
    image?: string
}

export interface RawMenu {
    [key: string]: Dish[]
}

export default class Menu {
    private readonly _rawData: RawMenu
    private readonly _dishes: Dictionary<Dish>

    constructor(rawData: RawMenu) {
        this._rawData = rawData || {}
        this._dishes = keyBy(flatten(values(this._rawData)), 'id')
    }

    get sections(): string[] {
        return Object.keys(this._rawData)
    }

    dishesInSection(section: string): Dish[] {
        return this._rawData[section] || []
    }

    getDishById(dishId: number): Dish | undefined {
        return this._dishes[dishId]
    }
}