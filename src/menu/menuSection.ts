import {html} from 'lit-html';
import dish from "./dish";
import {Component} from '../app';
import AppState from '../appState'
import {Dish} from './menu'

const menuSection: Component<AppState> = (app, section: string, dishes: Dish[]) => {
    return html`
        <section class="section box">
            <h2 class="subtitle">${section}</h2>
            <div class="columns is-multiline">
                ${dishes.map((d) => dish(app, d))}
            </div>
        </section>
    `
}

export default menuSection