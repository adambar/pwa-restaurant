import {html} from 'lit-html';
import {styleMap} from 'lit-html/directives/style-map';
import {Order} from "../orders/order";
import {successNotification} from "../utils/notification";
import {Component} from '../app'
import AppState from '../appState'
import {Dish} from './menu';

const dish: Component<AppState> = (app, dish: Dish) => {
    const [quantity, setQuantity] = app.useLocalState(dish, 1)

    const addToOrder = (quantity: number) => {
        const currentOrder = app.state.currentOrder || new Order()
        app.setState({
            currentOrder: currentOrder.withItemAppended(dish, quantity),
            notification: successNotification(`${quantity}× ${dish.name} added to your Order`)
        })
        setQuantity(1)
    }

    return html`
        <article class="column is-half-tablet is-one-third-desktop">
            <div class="media box">
                <figure class="media-left">
                    <p class="image is-64x64 is-clipped">
                        <img src=${dish.image || 'https://bulma.io/images/placeholders/128x128.png'}>
                    </p>
                </figure>
                <div class="media-content">
                    <strong>${dish.name}</strong><br/>
                    $${dish.price}
                </div>
                <div class="media-right field has-addons">
                    <div class="field">
                        <input type="number" 
                               class="input"
                               .value=${quantity}
                               @change=${(event: Event) => setQuantity(parseFloat((event.target as HTMLInputElement).value))}
                               style=${styleMap({width: '60px'})} />
                    </div>
                    <div class="field">
                        <button type="button" 
                                class="button is-primary"
                                @click=${() => addToOrder(quantity)}>Order</button>
                    </div>
                </div>
            </div>
        </article>
    `
}

export default dish