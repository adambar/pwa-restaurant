import generateRandomId from "./utils/generateRandomId";

function getUserId(): string {
    let existingId = localStorage.getItem('PWA.UserId')

    if (!existingId) {
        existingId = generateRandomId(12)
        localStorage.setItem('PWA.UserId', existingId)
    }

    return existingId
}

type HttpMethod = 'GET' | 'PUT'

async function doFetch(url: string, method: HttpMethod, body?: object) {
    const request = new Request(BaseUrl + url, {
        method,
        headers: {
            'Authorization': getUserId(),
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        },
        body: body ? JSON.stringify(body) : undefined
    })

    const response = await fetch(request)

    if (!response.ok) {
        throw new Error(`Unable to fetch: ${response}`)
    }

    return response.json()
}

const BaseUrl = "https://pwa-restaurant-backend.herokuapp.com"

export async function get<T>(url: string): Promise<T> {
    return doFetch(url, 'GET')
}

export async function put<T>(url: string, data: object): Promise<T> {
    return doFetch(url, 'PUT', data)
}