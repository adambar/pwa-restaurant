import {html} from 'lit-html';
import * as uuid from "uuid";

import orderFormLine from "./orderFormLine";
import {put} from "../apiClient";
import {successNotification} from "../utils/notification";
import App, {Component} from '../app'
import AppState from '../appState'

const onOrderPlaced = async (event: Event, app: App<AppState>) => {
    event.preventDefault()

    await put(`/orders/${uuid.v4()}`, app.state.currentOrder!)
    app.setState({
        currentOrder: undefined,
        notification: successNotification('Your order has been submitted.')
    })
}

const onOrderCleared = (event: Event, app: App<AppState>) => {
    event.preventDefault()
    app.setState({currentOrder: undefined})
}

const orderForm: Component<AppState> = (app) => {
    const currentOrder = app.state.currentOrder!

    return html`
        <form @submit=${(event: Event) => onOrderPlaced(event, app)} 
              @reset=${(event: Event) => onOrderCleared(event, app)}>
            <h2 class="title">Your Current Order</h2>
            
            <table class="table is-fullwidth">
                <tbody>
                    ${currentOrder.items.map((item) => orderFormLine(app, item))}
                </tbody>
            
                <tfoot>
                    <tr>
                        <th colspan="2">Total</th>
                        <th class="has-text-right">$${currentOrder.sumOfItems(app.state.menu)}</th>
                        <th></th>
                    </tr>
                </tfoot>
            </table>
            
            <div class="field is-grouped">
                <div class="control">
                    <button type="submit" class="button is-primary">Submit</button>
                </div>
                <div class="control">
                    <button type="reset" class="button is-text">Clear</button>
                </div>
            </div>
        </form>
    `
}

export default orderForm