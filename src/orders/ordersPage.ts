import {html} from 'lit-html';

import {get} from "../apiClient";
import orderRow from "./orderRow";
import orderForm from "./orderForm";
import {Component} from '../app'
import AppState from '../appState';
import {Order} from './order'

const ordersPage: Component<AppState> = (app) => {
    const [orders, setOrders] = app.useLocalState<Order[] | null>(ordersPage, null);

    app.useEffect(ordersPage, () => {
        get<Order[]>("/orders?take=10")
            .then((orders) => setOrders(orders))
    }, [app.state.currentOrder])

    return html`
        ${app.state.currentOrder && orderForm(app)}
        <hr/>
        <h1 class="title">Previous Orders</h1>
        
        ${orders
            ? (orders.length
                ? html`
                    <table class="table is-fullwidth">
                        ${orders.map((order) => orderRow(app, order))}
                    </table>
                `
                : html`<p>No orders yet.</p>`
            )
            : html`<p>Loading...</p>`
        }
    `
}

export default ordersPage