import Menu, {Dish} from '../menu/menu'

interface OrderItem {
    dishId: number
    dish?: Dish
    quantity: number
}

export class Order {
    items: OrderItem[]
    createdAt!: string
    totalPrice!: number

    constructor() {
        this.items = []
    }

    withItemReplaced(dish: Dish, replacedQuantity: number): this {
        let itemIdx = this.items.findIndex(i => i.dishId === dish.id)

        if (itemIdx === -1) {
            itemIdx = this.items.length
        }

        this.items[itemIdx] = {
            dishId: dish.id,
            quantity: replacedQuantity
        }

        return this
    }

    withItemAppended(dish: Dish, appendedQuantity: number): this {
        const currentItem = this.items.find(i => i.dishId === dish.id) || {quantity: 0}
        return this.withItemReplaced(dish, currentItem.quantity + appendedQuantity)
    }

    withoutItem(dish: Dish): this {
        let itemIdx = this.items.findIndex(i => i.dishId === dish.id)

        if (itemIdx !== -1) {
            this.items.splice(itemIdx, 1)
        }

        return this
    }

    sumOfItems(menu?: Menu): number {
        if (!menu) {
            return 0
        }

        return this.items.reduce((sum, item) => {
            return sum + (item.quantity * (menu.getDishById(item.dishId) || {price: 0}).price)
        }, 0)
    }
}