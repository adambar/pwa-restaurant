import {html} from 'lit-html';
import {styleMap} from "lit-html/directives/style-map";
import App, {Component} from '../app'
import AppState from '../appState'
import {Dish} from '../menu/menu'

const setNewDishQuantity = (app: App<AppState>, dish: Dish, quantity: number) => {
    app.setState({
        currentOrder: app.state.currentOrder!.withItemReplaced(dish, quantity)
    })
}

const removeDish = (app: App<AppState>, dish: Dish) => {
    app.setState({
        currentOrder: app.state.currentOrder!.withoutItem(dish)
    })
}

const orderFormLine: Component<AppState> = (app, item) => {
    const dish = app.state.menu && app.state.menu.getDishById(item.dishId)

    if (!dish) {
        return html``
    }

    return html`
        <tr>
            <td class="media">
                <figure class="media-left">
                    <p class="image is-64x64 is-clipped">
                        <img src=${dish.image || 'https://bulma.io/images/placeholders/128x128.png'}>
                    </p>
                </figure>
                <div class="media-content">
                    <strong>${dish.name}</strong><br/>
                    $${dish.price}
                </div>
            </td>
            <td>
                <div class="field">
                    <span style=${styleMap({padding: '5px 0', display: 'inline-block'})}>×</span>
                    <input type="number" 
                           class="input"
                           .value=${item.quantity}
                           @change=${(event: Event) => setNewDishQuantity(app, dish, parseFloat((event.target as HTMLInputElement).value))}
                           style=${styleMap({width: '60px'})} />
                </div>
            </td>
            <td class="has-text-right">
                <strong>$${item.quantity * dish.price}</strong>
            </td>
            <td>
                <button type="button"
                        class="delete"
                        @click=${() => removeDish(app, dish)}></button>
            </td>
        </tr>
    `
}

export default orderFormLine