import {html} from 'lit-html';
import dateFormat from 'dateformat'

import {Component} from '../app'
import AppState from '../appState'
import {Order} from './order'

const orderRow: Component<AppState> = (app, order: Order) => {
    return html`
        <tr>
            <td>
                ${order.items.map((item) => html`
                    ${item.quantity}× ${item.dish!.name}<br/>
                `)}
            </td>
            <td>${dateFormat(order.createdAt, 'dd.mm.yyyy, HH:MM')}</td>
            <td class="has-text-right">
                <strong>$${order.totalPrice}</strong>
            </td>
        </tr>   
    `
}

export default orderRow