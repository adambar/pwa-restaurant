import {precacheAndRoute, cleanupOutdatedCaches, createHandlerBoundToURL} from 'workbox-precaching'
import {registerRoute, setCatchHandler, NavigationRoute} from 'workbox-routing'
import {NetworkFirst, CacheFirst} from 'workbox-strategies'
import {CacheableResponsePlugin} from 'workbox-cacheable-response'

// listen to the incoming messages to detect when it's time to take over the control
self.addEventListener('message', (event) => {
    if (event.data === 'skip waiting') {
        self.skipWaiting()
    }
})

// precache all the static resources generated by webpack in the install event
precacheAndRoute(self.__WB_MANIFEST || [], {
    ignoreURLParametersMatching: [/.*/]
})

// remove the outdated caches in the activate event
cleanupOutdatedCaches();

// register the navigation route for SPA architecture
registerRoute(new NavigationRoute(createHandlerBoundToURL('/index.html')))

// network first for API GET calls (POSTs are not handled by Workbox by default)
registerRoute(
    /^https:\/\/pwa-restaurant-backend\.herokuapp\.com/,
    new NetworkFirst()
)

// cross-origin image routes - cache first
registerRoute(
    /.+\.(?:jpg|png)$/,
    new CacheFirst({
        plugins: [
            // make sure to cache opaque responses too
            new CacheableResponsePlugin({statuses: [0, 200]})
        ]
    })
)

// register fallback route for failed navigations (offline case)
setCatchHandler(({event}) => {
    if (event.request.destination === 'document') {
        return caches.match('/index.html')
    }

    return Response.error();
});
