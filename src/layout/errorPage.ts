import {html} from 'lit-html'

import AppState from '../appState'
import {Component} from '../app'

const errorPage: Component<AppState> = (app) => {
    return html`
        <h1>Error!</h1>
    `
}

export default errorPage