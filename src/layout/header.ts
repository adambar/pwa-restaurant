import {html} from 'lit-html';
import {classMap} from 'lit-html/directives/class-map';
import {styleMap} from 'lit-html/directives/style-map';

import AppState from '../appState'
import {Component} from '../app'

import logoPath from '../../assets/logo-header.png'

const header: Component<AppState> = (app) => {
    const {currentOrder, menu} = app.state;
    const currentPath = app.routeContext.path;
    const [menuExpanded, setMenuExpanded] = app.useLocalState(header, false)

    return html`
        <header class="navbar">
            <div class="navbar-brand">
                <h1 class="navbar-item title is-2" 
                    style=${styleMap({maxWidth: '90vw'})}>
                    <a href="/">
                        <img src=${logoPath} />
                        Pizza, Wine & Americano
                    </a>
                </h1>
    
                <button class="navbar-burger"
                    @click=${() => setMenuExpanded(!menuExpanded)} >
                    <span />
                    <span />
                    <span />
                </button>
            </div>
            <nav class=${classMap({'navbar-menu': true, 'is-active': !!menuExpanded})}>
                <div class="navbar-end">
                    ${(currentOrder && html`<div class="navbar-item">
                        <div class="tag is-info">${currentOrder.items.length} dishes ($${currentOrder.sumOfItems(menu)})</div>
                    </div>`) || ''}
                
                    <a href="/menu"
                        class=${classMap({'navbar-item': true, 'has-background-white-ter': currentPath.startsWith('/menu')})}>Menu</a>
                    <a href="/orders" 
                        class=${classMap({'navbar-item': true, 'has-background-white-ter': currentPath.startsWith('/orders')})}>Orders</a>
                </div>
            </nav>
        </header>`
}

export default header